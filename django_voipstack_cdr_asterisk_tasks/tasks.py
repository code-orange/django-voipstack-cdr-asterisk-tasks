from celery import shared_task
from django.db import connections
from django.db.utils import DataError

from django_voipstack_cdr_asterisk.django_voipstack_cdr_asterisk.asterisk_cdr_models import (
    Cdr,
    Cel,
)
from django_voipstack_cdr_asterisk.django_voipstack_cdr_asterisk.models import *
from django_voipstack_cdr_data.django_voipstack_cdr_data.models import *


def normalize_number(number: str):
    number = number.replace("#", "").replace(" ", "").replace(".", "").replace("*", "")
    new_number = number

    if number.startswith("00"):
        new_number = "+" + number[2:]
        # print('             ' + number + " need to be normalized (international) " + new_number)
    elif number.startswith("0137"):
        new_number = "+49" + number[1:]
        # print('             ' + number + " need to be normalized (votecall) " + new_number)
    elif number.startswith("0800"):
        new_number = "+49" + number[1:]
        # print('             ' + number + " need to be normalized (freecall) " + new_number)
    elif number.startswith("0"):
        new_number = "+49" + number[1:]
        # print('             ' + number + " need to be normalized (national) " + new_number)

    return new_number


@shared_task(name="voipstack_job_cdr_asterisk_sync")
def voipstack_job_cdr_asterisk_sync():
    # Connect to DBs
    all_ast_cdr_dbs = VoipCdrAsteriskDb.objects.all().order_by("id")

    database = str()

    for ast_cdr_db in all_ast_cdr_dbs:
        database = ast_cdr_db.name

        print("Processing DB: " + database)

        new_database = dict()
        new_database["id"] = database
        new_database["NAME"] = ast_cdr_db.db_dbase
        new_database["CONN_MAX_AGE"] = 30
        new_database["CONN_HEALTH_CHECKS"] = False
        new_database["ENGINE"] = "django.db.backends.mysql"
        new_database["USER"] = ast_cdr_db.db_user
        new_database["PASSWORD"] = ast_cdr_db.db_passwd
        new_database["HOST"] = ast_cdr_db.db_ip
        new_database["PORT"] = "3306"
        new_database["TIME_ZONE"] = "Europe/Berlin"
        new_database["AUTOCOMMIT"] = True
        new_database["OPTIONS"] = {"init_command": "SET sql_mode='STRICT_TRANS_TABLES'"}
        connections.databases[database] = new_database

        # Process CDR
        all_cdr = Cdr.objects.using(database).filter(lastapp="Dial").order_by("start")

        row_count = all_cdr.count()
        print("Found " + str(row_count) + " unprocessed CDR rows")

        if row_count < 1:
            ast_cdr_db.last_successful = datetime.now()
            ast_cdr_db.save()
            continue

        current_count = 1

        for entry in all_cdr:
            print("Processing " + str(current_count) + "/" + str(row_count))

            new_cdr = VoipCdr(
                ama_flags=entry.amaflags,
                account=entry.accountcode,
                num_src=normalize_number(entry.src[0:18]),
                num_dst=normalize_number(entry.dst[0:18]),
                call_start=entry.start,
                call_end=entry.end,
                call_answer=entry.answer,
                call_duration=entry.duration,
                call_billsec=entry.billsec,
                call_dispo=entry.disposition,
                userfield=entry.userfield,
                uniqueid=entry.uniqueid,
                linkedid=entry.linkedid,
                peeraccount=entry.peeraccount,
                cdr_sequence=entry.sequence,
                import_flag=0,
            )

            if entry.accountcode is not None:
                new_cdr.trunk = VoipTrunks.objects.get(trunk_name=entry.accountcode)
            elif entry.peeraccount is not None:
                new_cdr.trunk = VoipTrunks.objects.get(trunk_name=entry.peeraccount)

            # use higher resolution timers
            # CHAN_START
            try:
                event = entry.get_cel().using(database).get(eventtype="CHAN_START")
            except (Cel.DoesNotExist, Cel.MultipleObjectsReturned):
                pass
            else:
                new_cdr.call_start = event.eventtime

            # ANSWER
            try:
                event = entry.get_cel().using(database).get(eventtype="ANSWER")
            except (Cel.DoesNotExist, Cel.MultipleObjectsReturned):
                pass
            else:
                new_cdr.call_answer = event.eventtime

            if new_cdr.call_answer is not None:
                if new_cdr.call_answer.strftime("%Y") == "1970":
                    new_cdr.call_answer = None

            # HANGUP
            try:
                event = entry.get_cel().using(database).get(eventtype="HANGUP")
            except (Cel.DoesNotExist, Cel.MultipleObjectsReturned):
                pass
            else:
                new_cdr.call_end = event.eventtime

            # Calculate Duration
            new_cdr.call_duration = (
                new_cdr.call_end - new_cdr.call_start
            ).total_seconds()

            # Calculate Billsec
            if new_cdr.call_answer is not None:
                new_cdr.call_billsec = (
                    new_cdr.call_end - new_cdr.call_answer
                ).total_seconds()

            try:
                new_cdr.save(force_insert=True)
            except DataError:
                print("DataError skipped.")
            else:
                # Delete old events
                entry.get_cel().using(database).delete()

                # Delete source CDR
                entry.delete(using=database)

            current_count = current_count + 1

        ast_cdr_db.last_successful = datetime.now()
        ast_cdr_db.save()

    try:
        asterisk_sync = VoipCdrSyncState.objects.get(name="asterisk")
    except VoipCdrSyncState.DoesNotExist:
        asterisk_sync = VoipCdrSyncState(name="asterisk")

    asterisk_sync.sync_state_date = datetime.now()
    asterisk_sync.save()

    return
